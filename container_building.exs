alias Buildah.{Apk, Cmd, Print}

apk_catatonit_on_container = fn (container, options) ->
    Apk.packages_no_cache(container, [
        "catatonit"
    ], options)
    {catatonit_exec_, 0} = Cmd.run(container, ["sh", "-c", "command -v catatonit"])
    {_, 0} = Cmd.config(
        container,
        entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\"]"
    )
end

apk_catatonit_test = fn (container, image_ID, options) ->
    {_, 0} = Cmd.run(container, ["catatonit", "--version"], into: IO.stream(:stdio, :line))
    {_, 0} = Podman.Cmd.run(image_ID, ["catatonit", "--version"],
        tty: true, rm: true, into: IO.stream(:stdio, :line)
    )
    Apk.packages_no_cache(container, ["psmisc"], options)
    {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
    {_, 0} = Podman.Cmd.run(image_ID, ["pstree"],
        tty: true, rm: true, into: IO.stream(:stdio, :line)
    )
end




{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

# image_basename = "alpine:edge"
# _image = "localhost/" <> image_basename

# image_from = "docker.io/" <> image_basename
image_from = System.fetch_env!("IMAGE_FROM")
IO.puts(image_from)

# image = "localhost/" <> image_basename
# image = System.fetch_env!("IMAGE")
# IO.puts(image)

# image_to = "docker://#{System.fetch_env!("CI_REGISTRY_IMAGE")}/" <> image_basename
image_to = System.fetch_env!("IMAGE_TO")
IO.puts(image_to)

Buildah.from_push(
    image_from,
    apk_catatonit_on_container,
    apk_catatonit_test,
    image_to: image_to,
    quiet: System.get_env("QUIET")
)

Print.images()


# {container_, 0} = Cmd.from("docker.io/alpine:edge", quiet: true)
# container = String.trim(container_)
# {_, 0} = Cmd.run(container, [
#!!         "apk", "add", "--no-cache",  "--repository", "http://dl-cdn.alpinelinux.org/alpine/edge/testing", "--quiet",
#             "catatonit"
#     ],
#     into: IO.stream(:stdio, :line)
# )
# {catatonit_, 0} = Cmd.run(container, ["sh", "-c", "command -v catatonit"])
# {_, 0} = Cmd.config(
#     container,
#     entrypoint: "[\"#{String.trim(catatonit_)}\", \"--\"]"
# )
# {_, 0} = Cmd.inspect(container)

# {_, 0} = Cmd.commit(container, image: image, quiet: true)
# {image_ID_, 0} = Cmd.commit(container, quiet: true)
# image_ID = String.trim(image_ID_)

# {_, 0} = Cmd.inspect(image_ID, type: "image")
# {_, 0} = Cmd.images(into: IO.stream(:stdio, :line))
# {_, 0} = Cmd.run(container, ["catatonit", "--version"], into: IO.stream(:stdio, :line))
#!! {_, 0} = Podman.Cmd.run(image_ID, ["catatonit", "--version"],
#     tty: true, rm: true, into: IO.stream(:stdio, :line)
# )
# {_, 0} = Cmd.run(container, ["busybox", "pstree"], into: IO.stream(:stdio, :line))
#!! {_, 0} = Podman.Cmd.run(image_ID, ["busybox", "pstree"],
#     tty: true, rm: true, into: IO.stream(:stdio, :line)
# )
# {_, 0} = Cmd.push(image_ID, image_to: image_to, quiet: true) 
