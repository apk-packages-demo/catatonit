# catatonit

Container init. [pkgs.alpinelinux.org](https://pkgs.alpinelinux.org/contents?name=catatonit&arch=x86_64)

* Software with similar fonctionality: tini

# Improved version (at some point but no more)
* [pacman-packages-demo/catatonit](https://gitlab.com/pacman-packages-demo/catatonit)
